import base64

from yolo import detect
import cv2
import config as cf
import logging
import numpy as np
from pyzbar.pyzbar import decode
import correspond
from PIL import Image
import pickle

video_capture = cv2.VideoCapture(2)
video_capture.set(3, 640)
video_capture.set(4, 480)


def scan_qr(img):
    data_list = []
    for barcode in decode(img):
        # print(barcode.data)  # b'111111':b表示字节为单位
        data = barcode.data.decode('utf-8')
        data_list.append(data)
        # print("data:" + data)
        # print(barcode.rect, barcode.polygon)
        # pts = np.array([barcode.polygon], np.int32).reshape(-1, 1, 2)
        # cv2.polylines(img, [pts], True, (255, 123, 255), 5)
        # pts_2 = barcode.rect
        # cv2.putText(img, data, (pts_2[0], pts_2[1]), cv2.FONT_HERSHEY_SIMPLEX, 0.9, (255, 10, 255), 2)
    return data_list


def average_rgb(img):
    r_count, g_count, b_count = 0, 0, 0
    point_num = len(img) * len(img[0])
    for width in img:
        for height in width:
            r_count += height[0]
            g_count += height[1]
            b_count += height[2]
    RGB = (r_count / point_num, g_count / point_num, b_count / point_num)
    color = '#'
    for i in RGB:
        num = int(i)
        # 将R、G、B分别转化为16进制拼接转换并大写  hex() 函数用于将10进制整数转换成16进制，以字符串形式表示
        color += str(hex(num))[-2:].replace('x', '0').upper()
    # print(img)
    return color


def expand_elements(dictionary, Track_status):
    print(dictionary['img'].shape)
    height, width = dictionary['img'].shape[0], dictionary['img'].shape[1]
    xywh, img = dictionary['xywh'], dictionary['img']
    dictionary['qr_information'] = scan_qr(img)

    x1, x2 = int((xywh[0] - xywh[2] / 2.0) * width), int((xywh[0] + xywh[2] / 2.0) * width)
    y1, y2 = int((xywh[1] - xywh[3] / 2.0) * height), int((xywh[1] + xywh[3] / 2.0) * height)
    dictionary.update({"xxyy": dictionary.pop("xywh")})
    dictionary['xxyy'] = (x1, x2, y1, y2)
    dictionary['cut_img'] = dictionary['img'][y1:y2, x1:x2]  # 原点在图片左上角，先竖后横
    # cv2.imshow('img', dictionary['cut_img'])
    dictionary['color'] = average_rgb(dictionary['cut_img'])
    dictionary['tank_status'] = 'Manual' if Track_status else "Infrared"
    # 图片序列化Base64
    dictionary['img'] = base64.b64encode(cv2.imencode('.jpg', dictionary['img'])[1]).decode()
    dictionary['cut_img'] = base64.b64encode(cv2.imencode('.jpg', dictionary['cut_img'])[1]).decode()

    return dictionary


def start_detect(Track_status):
    ret, frame = video_capture.read()
    img_path = cf.sample_img_path + "/sample" + ".jpg"
    # cv2.imshow("img", frame)
    print(img_path)
    logging.info("path of saving img:" + img_path)
    try:
        cv2.imwrite(img_path, frame)  # 保存摄像头截图
    except BaseException:
        return False
    #video_capture.release()  # 释放摄像头
    #cv2.destroyAllWindows()  # 删除建立的全部窗口
    #test_path = cf.sample_img_path + "/test" + ".jpg"  # 测试用的图片
    out_list = detect.start_yolo_detect(img_path)  # detect内的路径需要调整
    # 输出 图像，类别，以及xywh
    if out_list==None:
        return False
    for item in out_list:
        correspond.send_request(expand_elements(item, Track_status))  # 发送字典
    #print(out_list)
    return True

if __name__ == '__main__':
    #start_detect(Track_status=False)
    vp = cv2.VideoCapture(2)
    vp.set(3, 640)
    vp.set(4, 480)
    while True:
        ret, frame = vp.read()
        cv2.imshow("img", frame)
        if cv2.waitKey(1) & 0xFF == ord('q'):
           break

    
