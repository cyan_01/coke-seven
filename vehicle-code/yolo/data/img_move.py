import os
import numpy
import tqdm

data_path = "../../datasets/TACO/data"
out_path = "../../datasets/TACO/image"
count = 0
if __name__ == '__main__':
    for i in tqdm.trange(15):
        batch = "batch_" + str(i + 1)
        list = os.listdir(os.path.join(data_path, batch))
        for img in list:
            os.rename(os.path.join(data_path, batch, img), os.path.join(data_path, batch, str(count).zfill(5) + ".jpg"))
            count += 1

