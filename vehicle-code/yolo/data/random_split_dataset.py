import os
from tqdm import tqdm
import random
import shutil

all_images_dir = "E:/树莓派履带车/Project/yolov5-TACO/data/All_images"
all_labels_dir = "./All_labels"
train_dir = "E:/树莓派履带车/Project/yolov5-TACO/data/images/train"
test_dir = "E:/树莓派履带车/Project/yolov5-TACO/data/images/val"
train_label = "./labels/train"
test_label = "./labels/val"


def data_split(full_list, ratio, shuffle=False):
    """
    数据集拆分: 将列表full_list按比例ratio（随机）划分为2个子列表sublist_1与sublist_2
    :param full_list: 数据列表
    :param ratio:     子列表1
    :param shuffle:   子列表2
    :return:
    """
    n_total = len(full_list)
    offset = int(n_total * ratio)
    if n_total == 0 or offset < 1:
        return [], full_list
    if shuffle:
        random.shuffle(full_list)
    sublist_1 = full_list[:offset]
    sublist_2 = full_list[offset:]
    return sublist_1, sublist_2


if __name__ == '__main__':
    imgs_list = os.listdir(all_images_dir)
    test, train = data_split(imgs_list, ratio=0.2, shuffle=True)

    for img in tqdm(test):
        # print(os.path.join(all_images_dir, img), os.path.join(test_dir, img))
        shutil.copyfile(os.path.join(all_images_dir, img), os.path.join(test_dir, img))
    img = None
    for img in tqdm(train):
        shutil.copyfile(os.path.join(all_images_dir, img), os.path.join(train_dir, img))
