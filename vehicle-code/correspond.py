import requests
import json
import config
import logging


# import pyperclip


def send_request(dic):
    # headers中添加上content-type这个参数，指定为json格式
    headers = {'Content-Type': 'application/json'}
    # print(json.dumps(dic))
    # pyperclip.copy(json.dumps(dic))
    # post的时候，将data字典形式的参数用json包转换成json格式。
    response = requests.post(url=config.server_address, headers=headers, data=json.dumps(dic))
    logging.info("Correspond response code:", response)
