#!/usr/bin/env python2
# -*- coding: utf-8 -*-
from aip import AipSpeech
import pygame
import Adafruit_PCA9685
from bottle import get, post, run, request
import  RPi.GPIO as GPIO
import time
import camera_detect


SensorRight = 16
SensorLeft = 12


PWMA = 18
AIN1   =  22
AIN2   =  27

PWMB = 23
BIN1   = 25
BIN2  =  24


BtnPin = 19
Gpin = 5
Rpin = 6

pwm = Adafruit_PCA9685.PCA9685()

servo_min = 150  
servo_max = 600  

def set_servo_pulse(channel, pulse):
    pulse_length = 1000000    # 1,000,000 us per second
    pulse_length //= 60       # 60 Hz
    print('{0}us per period'.format(pulse_length))
    pulse_length //= 4096     # 1./mjpg_streamer -i "input_uvc.so" -o "output_http.so -p 8090"2 bits of resolution
    print('{0}us per bit'.format(pulse_length))
    pulse *= 1000
    pulse //= pulse_length
    pwm.set_pwm(channel, 0, pulse)

def set_servo_angle(channel,angle):
    angle=4096*((angle*11)+500)/20000
    pwm.set_pwm(channel,0,int(angle))

# 频率设置为50hz，适用于舵机系统。
pwm.set_pwm_freq(50)
rdegree = 90
cdegree = 90
set_servo_angle(5,rdegree)  #底座舵机
set_servo_angle(4,cdegree)  # 顶部舵机

time.sleep(0.5)


#通过百度云申请的账号和密码
APP_ID='25085888'
API_KEY='1b99pNPMtRi5bcx22cBxNaII'
SECRET_KEY='5aM7splWIC9DaiHgNrF83kYYGGfTVGbX'


aipSpeech=AipSpeech(APP_ID,API_KEY,SECRET_KEY)

def robot_speech(content):
    text=content
    #在可选的参数中对语速，音量，人声进行调整，采用‘per’为0的女生听起来会更清晰
    result = aipSpeech.synthesis(text = text, 
                             options={'spd':5,'vol':100,'per':0,})
    #将合成的语音保存为文件
    if not isinstance(result,dict):
        with open('makerobo.mp3','wb') as f:
            f.write(result)  
    else:print(result)
    #树莓派自带的pygame播放
    pygame.mixer.init()
    pygame.mixer.music.load('/home/pi/source-raspberry/vehicle-code/makerobo.mp3')
    pygame.mixer.music.play()


def t_up(speed,t_time):
        L_Motor.ChangeDutyCycle(speed)
        GPIO.output(AIN2,False)#AIN2
        GPIO.output(AIN1,True) #AIN1

        R_Motor.ChangeDutyCycle(speed)
        GPIO.output(BIN2,False)#BIN2
        GPIO.output(BIN1,True) #BIN1
        time.sleep(t_time)
        
def t_stop(t_time):
        L_Motor.ChangeDutyCycle(0)
        GPIO.output(AIN2,False)#AIN2
        GPIO.output(AIN1,False) #AIN1

        R_Motor.ChangeDutyCycle(0)
        GPIO.output(BIN2,False)#BIN2
        GPIO.output(BIN1,False) #BIN1
        time.sleep(t_time)
        
def t_down(speed,t_time):
        L_Motor.ChangeDutyCycle(speed)
        GPIO.output(AIN2,True)#AIN2
        GPIO.output(AIN1,False) #AIN1

        R_Motor.ChangeDutyCycle(speed)
        GPIO.output(BIN2,True)#BIN2
        GPIO.output(BIN1,False) #BIN1
        time.sleep(t_time)

def t_left(speed,t_time):
        L_Motor.ChangeDutyCycle(speed)
        GPIO.output(AIN2,True)#AIN2
        GPIO.output(AIN1,False) #AIN1

        R_Motor.ChangeDutyCycle(speed)
        GPIO.output(BIN2,False)#BIN2
        GPIO.output(BIN1,True) #BIN1
        time.sleep(t_time)

def t_right(speed,t_time):
        L_Motor.ChangeDutyCycle(speed)
        GPIO.output(AIN2,False)#AIN2
        GPIO.output(AIN1,True) #AIN1

        R_Motor.ChangeDutyCycle(speed)
        GPIO.output(BIN2,True)#BIN2
        GPIO.output(BIN1,False) #BIN1
        time.sleep(t_time)    


def setup():
    GPIO.setwarnings(False)
    GPIO.setmode(GPIO.BCM)  # 按物理位置给GPIOs编号
    GPIO.setup(Gpin, GPIO.OUT)  # 设置绿色Led引脚模式输出
    GPIO.setup(Rpin, GPIO.OUT)  # 设置红色Led引脚模式输出
    GPIO.setup(BtnPin, GPIO.IN, pull_up_down=GPIO.PUD_UP)  # 设置输入BtnPin模式，拉高至高电平(3.3V)
    GPIO.setup(SensorRight, GPIO.IN)
    GPIO.setup(SensorLeft, GPIO.IN)

    GPIO.setup(AIN2, GPIO.OUT)
    GPIO.setup(AIN1, GPIO.OUT)
    GPIO.setup(PWMA, GPIO.OUT)

    GPIO.setup(BIN1, GPIO.OUT)
    GPIO.setup(BIN2, GPIO.OUT)
    GPIO.setup(PWMB, GPIO.OUT)


setup()
        

L_Motor= GPIO.PWM(PWMA,100)
L_Motor.start(0)

R_Motor = GPIO.PWM(PWMB,100)
R_Motor.start(0)


def main(status):
    global cdegree
    global rdegree
    if status == "front":
        print("front")
        t_up(50,0)
    elif status == "rear":
        print("rear")
        t_down(50,0)
    elif status == "left":
        print("left")
        t_left(50,0)
    elif status == "right":
        print("right")
        t_right(50,0)
    elif status == "stop":
        print("stop")
        t_stop(0)
    elif status == "up":
        cdegree -= 5
        set_servo_angle(4,cdegree)
        print("stop")
    elif status == "left_h":
        rdegree -= 5
        set_servo_angle(5,rdegree)  
        print("stop")
    elif status == "right_h":
        rdegree += 5
        set_servo_angle(5,rdegree)  
        print("stop")
    elif status == "down":
        cdegree += 5
        set_servo_angle(4,cdegree)
        print("stop")

    
    


#### 收到浏览器发来的指令。
@post("/cmd")
def cmd():
    adss = request.body.read().decode()
    print("按下了按钮:" + adss)
    
    main(adss)
    return "OK"


def random_drive():
    
    try:
        #传感器信号
        SR_2 = GPIO.input(SensorRight)
        SL_2 = GPIO.input(SensorLeft)
        #默认都为true直行
        if SL_2 == True and SR_2 == True:
            print("t_up")
            t_up(50, 0.5)
        #右侧感应到东西左转
        elif SL_2 == True and SR_2 == False:
            print("Left")
            t_left(50, 0.5)
        #左侧感应到东西右转
        elif SL_2 == False and SR_2 == True:
            print("Right")
            t_right(50, 0.5)
        #左右都有东西后退
        else:
            t_stop(0.3)
            t_down(50, 0.4)
            t_left(50, 0.5)
    except KeyboardInterrupt:  # 当按下Ctrl+C时，将执行子程序destroy()。
        GPIO.cleanup()
        exit()
        
def manual_detect(TS):
    print("manual")
    flag=False
    content='开始检测'
    robot_speech(content)
    t_stop(2)
    flag = camera_detect.start_detect(Track_status=TS )
    if flag:
        content='成功检测到垃圾'
        robot_speech(content)  
        time.sleep(3)  
    return "OK"
        
@post("/auto")      
def auto_drive():
    flag = request.body.read().decode()
    print(flag)
    if flag == 'hand':
        manual_detect(True)
    elif flag == 'auto': 
        for i in range(20):
            print("auto_model")
            random_drive()
            if (i+1)%10==0:
                manual_detect(False)
    return "OK"  
#### 开启服务器，端口默认8080。
run(host="192.168.137.47")

    
